<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cart | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<?php
		include'header.php';
		 // echo "<pre>";
		 // var_dump($_SESSION['cart_3']);
		//session_destroy();
	?>
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							if(!empty($_SESSION['cart_3'])){
								foreach ($_SESSION['cart_3'] as $key=>$value) {
									$total=$value['price'] * $value['qty'];							

						?> 
						<tr>
							<td class="cart_product">
								<a href=""><img style="width: 150px;" src="demo/<?php echo $value['img']?>"alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href=""><?php echo $value['name'] ?></a></h4>
								<p id="ID"><?php echo $value['ID'] ?></p>
							</td>
							<td class="cart_price">
								<p><?php echo $value['price'] ?></p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" > + </a>
									<input class="cart_quantity_input" type="text" name="quantity" 
											value="<?php echo $value['qty'] ?>" autocomplete="off" size="2">
									<a class="cart_quantity_down" > - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$<?php echo $total ?></p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" ><i class="fa fa-times"></i></a>
							</td>
						</tr>
						<?php
								}
							}
						?>

					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li id="sub_total">Cart Sub Total <span>$59</span></li>
							<li id="tax">Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li id="total">Total <span>$61</span></li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->

	<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2><span>e</span>-shopper</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="images/home/iframe1.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Circle of Hands</p>
								<h2>24 DEC 2014</h2>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="images/home/iframe2.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Circle of Hands</p>
								<h2>24 DEC 2014</h2>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="images/home/iframe3.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Circle of Hands</p>
								<h2>24 DEC 2014</h2>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="images/home/iframe4.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Circle of Hands</p>
								<h2>24 DEC 2014</h2>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="images/home/map.png" alt="" />
							<p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Service</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="">Online Help</a></li>
								<li><a href="">Contact Us</a></li>
								<li><a href="">Order Status</a></li>
								<li><a href="">Change Location</a></li>
								<li><a href="">FAQ’s</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Quock Shop</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="">T-Shirt</a></li>
								<li><a href="">Mens</a></li>
								<li><a href="">Womens</a></li>
								<li><a href="">Gift Cards</a></li>
								<li><a href="">Shoes</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Policies</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="">Terms of Use</a></li>
								<li><a href="">Privecy Policy</a></li>
								<li><a href="">Refund Policy</a></li>
								<li><a href="">Billing System</a></li>
								<li><a href="">Ticket System</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>About Shopper</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="">Company Information</a></li>
								<li><a href="">Careers</a></li>
								<li><a href="">Store Location</a></li>
								<li><a href="">Affillate Program</a></li>
								<li><a href="">Copyright</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-sm-offset-1">
						<div class="single-widget">
							<h2>About Shopper</h2>
							<form action="#" class="searchform">
								<input type="text" placeholder="Your email address" />
								<button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
								<p>Get the most recent updates from <br />our site and be updated your self...</p>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2013 E-SHOPPER Inc. All rights reserved.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	


    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>



	<script type="text/javascript">
		
		// buttom_up
		$("a.cart_quantity_up").click(function(){
			var get_qty = $(this).next("input.cart_quantity_input").val();
			var sum_qty = parseFloat(get_qty)+1;
			var get_price = $(this).closest("tr").find(".cart_price").text();
			var total= parseFloat(get_price)*sum_qty;
			$(this).next("input.cart_quantity_input").val(sum_qty);	
			$(this).closest("tr").find(".cart_total_price").text("$"+total);	
			var get_id = $(this).closest("tr").find("#ID").text();

			//total price
			var get_tax = $("#tax").find("span").text().split("$");
			var get_sub_total = $("#sub_total").find("span").text().split("$");
			var sum_total = parseFloat(get_price) + parseFloat(get_sub_total[1]);
			$("#sub_total").find("span").text("$"+sum_total);
			var sum = sum_total + parseFloat(get_tax[1]);
			$("#total").find("span").text("$"+sum);		
			$.ajax({
				method:"POST",
				url:"ajax_qty.php",
				data: {
					linh_qty: get_id,
					check : 'up'
				},
				success: function(response){
					console.log(response);
				}
			});
		})

		// buttom_down

		$("a.cart_quantity_down").click(function(){
			var get_qty = $(this).prev("input.cart_quantity_input").val();
			var sum_qty = parseFloat(get_qty)-1;
			var get_id = $(this).closest("tr").find("#ID").text();	
			//total price
			var get_price = $(this).closest("tr").find(".cart_price").text();
			var get_sub_total = $("#sub_total").find("span").text().split("$");
			var get_tax = $("#tax").find("span").text().split("$");
			var sum_total = parseFloat(get_sub_total[1]) - parseFloat(get_price) ;
			
			$("#sub_total").find("span").text("$"+sum_total);
			var sum = sum_total + parseFloat(get_tax[1]);
			$("#total").find("span").text("$"+sum);		




			if(sum_qty<1)
			{
				$(this).closest("tr").remove();
				$.ajax({
					method:"POST",
					url:"ajax_qty.php",
					data: {
						linh_delete: get_id,
						check: 'del'
					},
					success: function(response){
						console.log(response);
					}
				});
			}
			else
			{
				var get_price = $(this).closest("tr").find(".cart_price").text();
				var total= parseFloat(get_price)*sum_qty;
				$(this).prev("input.cart_quantity_input").val(sum_qty);	
				$(this).closest("tr").find(".cart_total_price").text("$"+total);	
				$.ajax({
					method:"POST",
					url:"ajax_qty.php",
					data: {
						linh_qty_down: get_id,
						check: 'down'
					},
					success: function(response){
						console.log(response);
					}
				});				
			}
					
			
		})

		// delete

		$("a.cart_quantity_delete").click(function(){
			$(this).closest("tr").remove();
			var get_id_delete = $(this).closest("tr").find("#ID").text();

			//total price
			var get_tax = $("#tax").find("span").text().split("$");
			var get_price = $(this).closest("tr").find(".cart_total_price").text().split("$");
			var get_sub_total = $("#sub_total").find("span").text().split("$");
			var sum_total = parseFloat(get_sub_total[1]) - parseFloat(get_price[1]) ;
			var sum = sum_total + parseFloat(get_tax[1]);
			$("#sub_total").find("span").text("$"+sum_total);
			$("#total").find("span").text("$"+sum);		


			var count=$("#count").find("span").text();
			var sum_cart= parseFloat(count) - 1;
			$("#count").find("span").text(sum_cart);

			$.ajax({
				method:"POST",
				url:"ajax_qty.php",
				data: {
					linh_delete: get_id_delete,
					check: 'del'
				},
				success: function(response){
					console.log(response);
				}
			});

		})

		$(document).ready(function(){
			var get_sub_total = $("#sub_total").find("span").text();
			var get_tax = $("#tax").find("span").text().split("$");
			var get_total = $(".cart_total_price").text().split("$");
			var a=[];
			a=get_total
			var sum=0;
			for(var i=0; i<a.length; i++){
				sum +=parseFloat(a[i])<<0;
			}	
			$("#sub_total").find("span").text("$"+sum);
			var sum_total = sum + parseFloat(get_tax[1]);
			$("#total").find("span").text("$"+sum_total);
			$.ajax({
				method:"POST",
				url:"ajax_total.php",
				data: {
					linh_total: sum_total,
					linh_sum:sum
				},
				success: function(response){
					console.log(response);
				}
			});
		
		})
	</script>
</body>
</html>