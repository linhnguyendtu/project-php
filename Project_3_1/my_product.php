<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<?php 
		include 'header.php';
	?>
	<?php
		if(!empty($_SESSION['cart_3'])){
		$sql = "SELECT * FROM `product` WHERE `ID_User`='".$_SESSION['id']."' ORDER BY `ID`";
 		$result= $con->query($sql);
 		$data=[];
 		if($result->num_rows>0){
 			 while ($row = $result->fetch_assoc()) {
                    $data[] = $row;
                }
        }
		}
 		

 	?>

	<!-- ACCOUNT -->
	<div class="col-sm-3">
		<div class="left-sidebar">
			<h2>ACCOUNT</h2>
			<div class="panel-group category-products" id="accordian"><!--category-productsr-->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordian" href="user_update.php">
								<span class="badge pull-right"><i class="fa fa-plus"></i></span>
								ACCOUNT
							</a>
						</h4>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordian" href="my_product.php">
								<span class="badge pull-right"><i class="fa fa-plus"></i></span>
								MYPRODUCT
							</a>
						</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- PRODUCTS -->

	<div class="col-sm-9">
		<div class="table-responsive cart_info">
			<style type="text/css">
				.cart_menu {
				    background: #FE980F;
				    color: #fff;
				    font-size: 16px;
				    font-family: 'Roboto', sans-serif;
				    font-weight: normal;
				}
				#submit{
					background: #e67e22;
					float: right;
					color: #ecf0f1;
				}
			</style>
		<form method="post">
			<table class="table table-condensed">
				<thead>
					<tr class="cart_menu">
						<td class="id">ID</td>
						<td class="Name">Name</td>
						<td class="Image">Image</td>
						<td class="Price">Price</td>
						<td class="Action">Action</td>
						<td></td>
					</tr>
				</thead>
				<?php
					foreach ($data as $value) {

				?>
			<form method="post">
				<tbody>
					<tr>
						<td class="cart_product">
							<a href=""> <?php echo $value['ID'];?></a>
						</td>
						<td class="cart_description">
							<h4><a href=""><?php echo $value['name'];  ?></a></h4>
						</td>
						<td class="cart_img">
							<p><img style="width: 100px" src="demo/<?php echo $value['img'];?>"></p>
						</td>
						<td class="cart_price">
							<p><?php echo $value['price'];  ?></p>
						</td>
					  	<td class="edit">
					  		<a href="edit_product.php?id=<?php echo $value['ID']?>">Edit</a>
					  	</td>	
						<td class="cart_delete">							
							<a class="cart_quantity_delete" href="delete.php?id=<?php echo $value['ID']?>"><i class="fa fa-times"></i></a>
						</td>
					</tr>
				</tbody>
			</form>
				<?php

					};
				?>
			</table>
			<button id="submit" type="submit" name="add_new" > <a style=" color: white " href="create_product.php">Add New</a></button>
			</div>
		</div>
	</form>

</body>
</html>