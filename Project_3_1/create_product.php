<!DOCTYPE html>
<html>
	<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<?php 
		include 'header.php'
	?>
	<?php
		$name_er="";
		$price_er="";
		$img_er="";
		$check=0;
		
	   
	if(isset($_POST['submit'])){

		if(empty($_POST['name'])){
			$name_er="Please add name";
			$check=1;
		}

		if(empty($_POST['price'])){
			$price_er="Please add price";
			$check=1;
		}

		if(!empty($_FILES['img']['name'])){
			$name=explode(".",$_FILES['img']['name']);
			$img=['jpg', 'png', 'JPEG', 'JPG', 'PNG'];

			//check dung luong file
			if($_FILES['img']['size']> 5000000000)
			{
				$img_er= "File lon hon dung luong cho phep";
				$check=1;
			}

			// check  file phai hinh anh khong
			else if(in_array($name[1], $img)==false)
			{
				$img_er= "File khong phai la hinh anh";
				$check=1;
			}

			//check loi file upload
			else if($_FILES['img']['error'] > 0 )
			{
				$img_er= "Upload file error";
				$check=1;
			}
		}

		if($check==0){
			$sql= "INSERT INTO product (name,price,img,ID_User)
				VALUES ('".$_POST['name']."',
						'".$_POST['price']."',
						'".$_FILES['img']['name']."',
						'".$_SESSION['id']."') ;";
			if($result = $con->query($sql)){
				move_uploaded_file($_FILES['img']['tmp_name'], './demo/'.$_FILES['img']['name']);
				echo " Creat done!! click <a href='my_product.php'> here </a> to return";
			}
			else
				echo "False";
		}
	}
	?>
	<div class="col-sm-3">
		<div class="left-sidebar">
			<h2>ACCOUNT</h2>
			<div class="panel-group category-products" id="accordian"><!--category-productsr-->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
								<span class="badge pull-right"><i class="fa fa-plus"></i></span>
								ACCOUNT
							</a>
						</h4>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordian" href="my_product.php">
								<span class="badge pull-right"><i class="fa fa-plus"></i></span>
								MYPRODUCT
							</a>
						</h4>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-9">
		<div class="signup-form">
			<h2>Creat Product</h2>
			<style type="text/css">
				p {
					color: red;
				}
			</style>		
			<form method="post" action="#" enctype="multipart/form-data">
				<input type="name" name="name"  placeholder="name" />
						<p> <?php echo $name_er ?>
				<input type="price" name="price"  placeholder="price" />
						<p> <?php echo $price_er ?>
				<input type="file" name="img" placeholder="No file"/>
						<p> <?php echo $img_er ?>
				<button type="submit" name="submit" class="btn btn-default">ADD</button>
			</form>

		</div>
	</div>

</body>
</html>

