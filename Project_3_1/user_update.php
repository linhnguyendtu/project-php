<!DOCTYPE html>
<html>
	<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<?php 
		include 'header.php'
	?>
	<div class="col-sm-3">
		<div class="left-sidebar">
			<h2>ACCOUNT</h2>
			<div class="panel-group category-products" id="accordian"><!--category-productsr-->
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
								<span class="badge pull-right"><i class="fa fa-plus"></i></span>
								ACCOUNT
							</a>
						</h4>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordian" href="my_product.php">
								<span class="badge pull-right"><i class="fa fa-plus"></i></span>
								MYPRODUCT
							</a>
						</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
		$notif_name="";
		$notif_email="";
		$notif_pass="";
		$notif_adress="";
		$notif_nation="";
		$notif_phone="";
		$check=0;
		$sql = "SELECT * FROM `users` WHERE `ID`='".$_SESSION['id']."' ";
		$result =$con->query($sql);
		$data_user=[];
		if($result->num_rows >0) {
			while ($row = $result->fetch_assoc()) {
                    $data_user[] = $row;
            }
        }
        if(isset($_POST['submit'])){
        	if(empty($_POST['email'])){
        		$notif_email="Please add email!!";
        		$check=1;
        	}
        	if(empty($_POST['name'])){
        		$notif_email="Please add name!!";
        		$check=1;
        	}
        	if(empty($_POST['pass'])){
        		$notif_email="Please add pass!!";
        		$check=1;
        	}
        	if(!empty($_FILES['images']['name'])){
        		$name=explode('.', $_FILES['images']['name']);
        		$img=['jpg', 'png', 'JPEG', 'JPG', 'PNG'];

        		if($_FILES['images']['size']>5000000000){
        			echo "Erorr";
        			$check=1;
        		}
        		else if(in_array($name[1], $img)==false){
        			echo "Erorr";
        			$check=1;
        		}
        		else if($_FILES['images']['error']>0){
        			echo "Erorr";
        			$check=1;
        		}

        	}
        	if($check==0){
        		move_uploaded_file($_FILES['images']['tmp_name'],'./avatar/'. $_FILES['images']['name']);
        		$sql=" UPDATE `users` SET 
        				`email`='".$_POST['email']."',
        				`name` ='".$_POST['name']."',
        				`pass` ='".md5($_POST['pass'])."',
        				`avatar`='".$_FILES['images']['name']."'
        				WHERE `ID` = '".$id_user."' "; 
        		if($result=$con->query($sql)){
        			echo "Update done!! Click <a href='index.php'> here </a> to return";
        		}     
        		else
        			echo "Update false!!";				      				      		
        	}
        }		
	?>
		<div class="col-sm-9">
			<div class="signup-form">
				<h2>User Update</h2>
				<?php 
					foreach ($data_user as $value) {
				?>
				<style type="text/css">
					p {
						color: red;
					}
				</style>		
				<form method="post" action="#" enctype="multipart/form-data">
					<input type="email" name="email"  placeholder="Email Address" value="<?php echo $value['email']; ?>" />
						<p> <?php echo $notif_email ?>
					<input type="password" name="pass" placeholder="Password" value="<?php echo $value['email'];  ?>" />
						<p> <?php echo $notif_pass ?>
					<input type="text" name="name" placeholder="Name" value="<?php echo $value['name'];  ?>"/>
						<p> <?php echo $notif_name ?>					
					<input type="file" name="images" placeholder="No file" value="<?php echo $value['avatar'];  ?>"/>

					<button type="submit" name="submit" class="btn btn-default">Update</button>
				</form>
				<?php
					};
				?>

			</div>
		</div>

</body>
</html>

